var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var Schema = mongoose.Schema;
require('dotenv').config()

const ColorSchema = new Schema({
    // product cat
    color: String,
    createdAt: {type: Date, default: Date.now}
}, { collection: 'colors' });

const Color = mongoose.model('color', ColorSchema)
module.exports = Color;