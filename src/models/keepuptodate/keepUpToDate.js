var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var Schema = mongoose.Schema;
require('dotenv').config()

const Email = require('mongoose-type-mail');


const subscribeUsersSchema = new Schema({
    // product cat
    email: {type:Email},
    status: String,
    createdAt: {type: Date, default: Date.now}
}, { collection: 'subscribeusers' });

const SubscribeUsers = mongoose.model('subscribeuser', subscribeUsersSchema)
module.exports = SubscribeUsers;