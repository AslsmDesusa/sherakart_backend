var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var Schema = mongoose.Schema;
const async = require('async');
require('dotenv').config()

const orderSchema = new Schema({
    // product cat
    paymentType: String,
    status: Object,
    Address: Object,
    productDetails: Array,
    createdAt: {type: Date, default: Date.now}
}, { collection: 'orders' });

const Order = mongoose.model('order', orderSchema)
module.exports = Order;