var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var Schema = mongoose.Schema;
const async = require('async');
require('dotenv').config()
const JWT = require('jsonwebtoken');

const Email = require('mongoose-type-mail');

const MainSchema = new Schema({
    CompoCateName: String, 
    productName: String,
    productPrice: String,
    productDiscount: String,
    productActualPrice: String,
    productDescription: String,
    productCoverImage: String,
    createdAt: {type: Date, default: Date.now}
}, { collection: 'mainSliderImage' });

const mainProduct = mongoose.model('Product', MainSchema)
module.exports = mainProduct;