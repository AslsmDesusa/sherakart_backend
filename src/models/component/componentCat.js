var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var Schema = mongoose.Schema;
const async = require('async');
require('dotenv').config()

const ComCatSchema = new Schema({
    CompoCateName: String,
    createdAt: {type: Date, default: Date.now}
}, { collection: 'componentesCat' });

const componentCat = mongoose.model('Component', ComCatSchema)
module.exports = componentCat;