var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var Schema = mongoose.Schema;
const async = require('async');
require('dotenv').config()

const ProductSchema = new Schema({
    productType: String,
    productCateName: String,
    createdAt: {type: Date, default: Date.now}
}, { collection: 'productsCat' });

const productCat = mongoose.model('ProductCat', ProductSchema)
module.exports = productCat;