var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var Schema = mongoose.Schema;
const async = require('async');
require('dotenv').config()
const JWT = require('jsonwebtoken');

const Email = require('mongoose-type-mail');

const ProductSchema = new Schema({
    // product cat
    typeOfWear: String,
    productCat: String,
    productName: String,
    productPrice: String,
    productDiscount: String,
    productActualPrice: String, 
    productGender: String,
    productDescription: String,
    productColor: Array,
    productSizes: Array,
    productQuntity: String,
    favorite: {type: String, enum: ['favorite', 'favorite_border', ''], default: 'favorite_border'},
    productRelativeImages: Array,
    createdAt: {type: Date, default: Date.now}
}, { collection: 'products' });

const Product = mongoose.model('product', ProductSchema)
module.exports = Product;