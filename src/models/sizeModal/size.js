var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var Schema = mongoose.Schema;
require('dotenv').config()

const SizeSchema = new Schema({
    // product cat
    wearComponent: String,
    size: String,
    createdAt: {type: Date, default: Date.now}
}, { collection: 'sizes' });

const Size = mongoose.model('size', SizeSchema)
module.exports = Size;