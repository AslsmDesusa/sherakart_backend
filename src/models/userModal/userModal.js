var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const async = require('async');
require('dotenv').config()
const JWT = require('jsonwebtoken');

const Email = require('mongoose-type-mail');

const UserSchema = new Schema({
    userDetails:{
        firstName: String,
        lastName: String,
        gender: {type: String, enum: ['Male', 'Female', ''], default: 'Male'},
    },
    privateDetails:{
        email: {type:Email},
        password: String,
        phoneNumber: String,
    },
    // Delivery Info
    buyerAddress: Array,
    // items with there ID's
    begItems: Array,
    favoriteList: Array,
    orderList: Array,
    // process of signup & it fields
    role: {type: String, enum: ['Admin', 'User'], default: 'User'},
    isValidUser:Boolean,
    whyNotValid: String,
    isLogin: Boolean,
    otp: String,
    expireAt: {
        type: Date, 
        default: Date.now,
        index: { expires: "2m" }
    },
    createAt: String,
}, { collection: 'users' });

UserSchema.statics.login = async function (username, password) {
    var query = { $or : [ { 'privateDetails.email' : username }, { 'privateDetails.phoneNumber' : username } ] }
    const user = await this.findOne(query).exec();
    if (!user) return false;
    const doesMatch = await bcrypt.compare(password, user.privateDetails.password);
    return doesMatch ? user : false;
};

UserSchema.statics.generateToken = function(id){
    const token = JWT.sign(id.toJSON(), process.env.API_SECRET_KEY);
    return token
}

UserSchema.pre('save', function (next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('privateDetails.password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) return next(err);

        // hash the password along with our new salt
        bcrypt.hash(user.privateDetails.password, salt, function (err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.privateDetails.password = hash;
            next();
        });
    });
});

const User = mongoose.model('User', UserSchema)
module.exports = User;