var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var Schema = mongoose.Schema;
require('dotenv').config()

const AddressSchema = new Schema({
    // product cat
    RecieverName: String,
    PhoneNumber: String,
    AlternateNo: String,
    PinCode: String,
    Address: String,
    Locality: String,
    Landmark: String,
    City: String,
    State: String,
    Country: String,
    ThisIsMy: String,
    createdAt: {type: Date, default: Date.now}
}, { collection: 'addresses' });

const Address = mongoose.model('address', AddressSchema)
module.exports = Address;