function myFunction() {
    var dataId = document.getElementById("totalProductPrice").innerText;
    var convert = parseInt(dataId) * 100

    var options = {
        "key": "rzp_test_OWhANKQMlGGS7j",
        "amount": convert, // 2000 paise = INR 20
        "name": "Sherakart",
        "description": "Purchase Description",
        "image": "./favicon.ico",
        "handler": function (response){
            if(response){
                localStorage.setItem('paymentId', response.razorpay_payment_id);
                document.getElementById("myBtn").click();
            }else{
                alert('payment was canceled');
            }
        },
        "prefill": {
            "name": "",
            "email": ""
        },
        "notes": {
            "address": "Hello World"
        },
        "theme": {
            "color": "#FF9800"
        }
    };
    var rzp1 = new Razorpay(options);
    rzp1.open();
}
