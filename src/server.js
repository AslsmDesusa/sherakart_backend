'use strict';

// import
import Login from './www/Login&Reg/login'
import CoverImageUpload from './www/Component/comopnentData'
import color from './www/Component/color'
import size from './www/Component/size'
import EditUserData from './www/Login&Reg/updateUserData'
import addCompoCat from './www/Component/componentCat'
import addProductCat from './www/product/uploadProductCat'
import addProduct from './www/product/uploadProduct'
import FavoriteItem from './www/favoriteAndBeg/favoriteItem'
import updateAddress from './www/Login&Reg/addAddress'
import bag from './www/favoriteAndBeg/bagitem'
import googleAuth from './www/googleAuth'
import paymentProcess from './www/payment.js/paymentProcess'
import filter from './www/productFilter/filterProduct'
import subscribeUser from './www/subscribeUser/subscribe'

const Hapi = require('@hapi/hapi');
const Path = require('path');

//db connection established
const db = require('../database').db;

const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');


const port = process.env.PORT || 8000
const { NODE_ENV } = process.env


// validation function
const validate = async function (user, decoded, request) {
    // checks to see if the person is valid
    if (!user['_id']) {
        return { isValid: false };
    }
    else {
        return { isValid: true };
    }
};

const init = async () => {

    const server = new Hapi.Server({
        port: port,
        routes: { cors: true }
    });

    const swaggerOptions = {
        info: {
            title: 'Test API Documentation'
        },
    };

    // register hapi swagger documentation
    await server.register([
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: swaggerOptions
        }
    ]);

    // using cookie
    await server.register(require('hapi-auth-cookie'));
    server.auth.strategy('token', 'cookie', {
        cookie: {
            name: 'sherakart',

            // Don't forget to change it to your own secret password!
            password: 'vZiYpmTzqXMp8PpYXKwqc9ShQ1UhyAfy',

            // For working via HTTP in localhost
            isSecure: false
        },

    });

    
    //register hapi-auth-jwt2 for authentication
    await server.register(require('hapi-auth-jwt2'));
    server.auth.strategy('jwt', 'jwt',
    {
        key: 'vZiIpmTzqXHp8PpYXTwqc9SRQ1UAyAfC',
        validate: validate,
        verifyOptions: { algorithms: ['HS256'] }
    });

    //register all routes
    await server.route(Login);
    await server.route(CoverImageUpload);
    await server.route(EditUserData)
    await server.route(addCompoCat)
    await server.route(addProductCat)
    await server.route(addProduct)
    await server.route(FavoriteItem)
    await server.route(updateAddress)
    await server.route(bag)
    await server.route(googleAuth)
    await server.route(paymentProcess)
    await server.route(color)
    await server.route(size)
    await server.route(filter)
    await server.route(subscribeUser)

    // if (process.env.NODE_ENV === 'production') {

    //     // Static Folder
    //     await server.route({
    //         method: 'GET',
    //         path: '/{p*}',
    //         handler: {
    //             directory: {
    //                 path: Path.join(__dirname, 'public')
    //             }
    //         }
    //     });

    //     // handle SPA
    //     await server.route({
    //         method: 'GET',
    //         path: '/.*/',
    //         handler: {
    //             file: './public/index.html'
    //         }});
    // }

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();
