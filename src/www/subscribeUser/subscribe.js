const subscribe = require('../../models/keepuptodate/keepUpToDate')
const ObjectID = require('mongodb').ObjectID


require('dotenv').config();

const routes = [
    {
        method: 'GET',
        path: '/api/get-all-suscribers',
        config: {
        // include this route in swagger documentation
            description:"Home suscribers",
            notes:"Home suscribers",
            tags:['api'],
        },
        handler: async(request, h)=>{
            let pr = async (resolve, reject) => {
                subscribe.find().limit(20).skip(20 * request.query.count)
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'get all subscribers',
                        data: res
                    })
                })
            }
            return new Promise(pr)
        }
    },
    {
        method: 'DELETE',
        path: '/api/delete-suscribers',
        config: {
        // include this route in swagger documentation
            description:"Home suscribers",
            notes:"Home suscribers",
            tags:['api'],
        },
        handler: async(request, h)=>{
            let pr = async (resolve, reject) => {
                subscribe.findOneAndDelete({_id: ObjectID(request.query.id)})
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'Subscriber deleted successfully',
                    })
                })
            }
            return new Promise(pr)
        }
    },
    {
        method: 'POST',
        path: '/api/subscribe',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"user can subscribe website",
            notes:"user can subscribe website"
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                const newSubscriber = new subscribe({
                    email: request.payload.email,
                    status: 'subscribed'
                })
                subscribe.findOne({email: request.payload.email})
                .then(function(result){
                    if (result) {
                        return resolve({
                            status: 'error',
                            message: 'user already subscribed'
                        })
                    }else{
                        newSubscriber.save()
                        .then(function(res){
                            return resolve({
                                status: 'success',
                                message: 'Thank you for subscribed you will be notify when we upload new products'
                            })
                        })
                        .catch(err=>{
                            throw err
                        })
                    }
                })
                .catch(err=>{
                    throw err
                })
            }
            return new Promise(pr)
        }  
    }
]
export default routes;