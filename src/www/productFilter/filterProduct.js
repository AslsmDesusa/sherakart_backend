const productData = require('../../models/productModal/Product')
const ObjectID = require('mongodb').ObjectID

const Joi = require('joi');

require('dotenv').config();

const routes = [
    {
        method: 'POST',
        path: '/api/filter',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"filtering product by user",
            notes:"filtering product by user"
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                var query = {$or:[{ productSizes: { $elemMatch: { $in: request.payload.size }}}, { productColor: { $elemMatch: { $in: request.payload.color }}}, { productGender: { $in: request.payload.gender } }]}
                productData.find(query)
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'data Filterd successfully',
                        data: res
                    })
                })
                .catch(err=>{
                    throw err
                })
            }
            return new Promise(pr)
        }  
    }
]
export default routes;