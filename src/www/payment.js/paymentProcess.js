const userModal = require('../../models/userModal/userModal')
const orderModal = require('../../models/orderModal/orderModal')
const ObjectID = require('mongodb').ObjectID

const Joi = require('joi');

require('dotenv').config();

const routes = [
    {
        method: 'GET',
        path: '/api/search-orders',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"search orders",
            notes:"search order",
        },
        handler: async (request, h) => {
            var query = { $or : [ {'Address.RecieverName':{$regex: request.query.RecieverName, $options: 'i'}}, {'Address.PhoneNumber':{$regex: request.query.PhoneNumber, $options: 'i'}}, {'Address.Address':{$regex: request.query.Address, $options: 'i'}}]}
            let pr = async (resolve, reject)=>{
                orderModal.find(query).limit(20).skip(20 * request.query.count)
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'Order List get successfully',
                        data: res
                    })
                })
                .catch(err=>{
                    throw err
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'GET',
        path: '/api/get/all/orders',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"getting all order data",
            notes:"getting all order data",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                orderModal.find()
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'Order List get successfully',
                        data: res
                    })
                })
                .catch(err=>{
                    throw err
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'GET',
        path: '/api/get/user/orders',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"getting user order data",
            notes:"getting user order data",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                userModal.findOne({_id: ObjectID(request.query._id)})
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'Order List get successfully',
                        data: res.orderList
                    })
                })
                .catch(err=>{
                    throw err
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'POST',
        path: '/api/add/order/list',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"API Adding payment data",
            notes:"API Adding payment data",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                const paymentData = request.payload.paymentData
                userModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$push: {orderList: paymentData}})
                .then(function(response){
                    userModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$unset: {'begItems': []}})
                    .then(function(res){
                        const newOrder = new orderModal({
                            paymentType: request.payload.paymentType,
                            status: 'Customer Order',
                            Address: request.payload.Address,
                            productDetails: request.payload.productDetails
                        })
                        newOrder.save()
                        .then(function(res){
                            return resolve({
                                status: 'success',
                                message: 'Order successfully done'
                            })
                        })
                        .catch(err=>{
                            throw err
                        })
                    })
                })
                .catch(err=>{
                    throw err
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'GET',
        path: '/api/unset/order/list',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"API Adding payment data",
            notes:"API Adding payment data",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                userModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$unset: {'orderList': []}})
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'history clear successfully'
                    })
                })
            }
            return new Promise(pr)
        }  
    }    
]
export default routes;