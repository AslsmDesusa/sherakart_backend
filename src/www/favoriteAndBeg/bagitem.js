const userModal = require('../../models/userModal/userModal')
const ObjectID = require('mongodb').ObjectID

const Joi = require('joi');

require('dotenv').config();

const routes = [
    {
        method: 'GET',
        path: '/api/bag/list',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"API Favorite list",
            notes:"API favorite List",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                userModal.findOne({_id: ObjectID(request.query._id)})
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'Bag List',
                        Data: res.begItems
                    })
                })
                .catch(err=>{
                    return reject(err)
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'GET',
        path: '/api/remove/bag/item',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"API Favorite list",
            notes:"API favorite List",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                userModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, { $pull: { 'begItems': { _id: request.query.productId } } })
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'Bag product remove successfully',
                    })
                })
                .catch(err=>{
                    return reject(err)
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'POST',
        path: '/api/save/for/later/item',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"API Favorite list",
            notes:"API favorite List",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                var favoriteList = request.payload
                userModal.findOne({_id: ObjectID(request.query._id)}, {favoriteList: { $elemMatch: {_id: request.query.productId}}}, {"favoriteList.$": 1})
                .then(function(res){
                    if (res) {
                        return resolve({
                            status: 'error',
                            message: 'Product already in Favorite List',
                        })
                    }else{
                        userModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$push: {favoriteList: favoriteList}})
                        .then(function(response){
                            userModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, { $pull: { 'begItems': { _id: request.query.productId } } })
                            .then(function(result){
                                return resolve({
                                    status: 'success',
                                    message: 'product saved in favorite List'
                                })
                            })
                            .catch(err=>{
                                throw err
                            })
                        })
                        .catch(err=>{
                            throw err
                        })
                    }
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'POST',
        path: '/api/move/to/beg',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"storing data into Bag List",
            notes:"storing data into Bag List",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                const bagItem = request.payload
                userModal.findOne({_id: ObjectID(request.query._id)}, {begItems: { $elemMatch: {_id: request.query.productId}}}, {"begItems.$": 1})
                .then(function(response){
                    if (response.begItems.length === 0) {
                        userModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$push: {begItems: bagItem}})
                        .then(function(response){
                            return resolve({
                                status: 'success',
                                message: 'Move to bag successfully',
                                data: response
                            })
                        })
                        .catch(err=>{
                            throw (err)
                        })
                    }else{
                        return resolve({
                            status: 'error',
                            message: 'product already in bag',
                        })
                    }
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'PUT',
        path: '/api/update/quantity',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"change quantity",
            notes:"change quantity",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                userModal.findOneAndUpdate({_id : ObjectID(request.query._id), begItems: { $elemMatch: { _id: request.query.productId } }} , { $set: { "begItems.$.Qty": request.payload.Qty, "begItems.$.footSize": request.payload.footSize }})
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'you have increase Qty of this product'
                    })
                })
                .catch(err=>{
                    return reject(err)
                })
            }
            return new Promise(pr)
        }  
    }    
]
export default routes;