const userModal = require('../../models/userModal/userModal')
const ObjectID = require('mongodb').ObjectID

const Joi = require('joi');

require('dotenv').config();

const routes = [
    {
        method: 'GET',
        path: '/api/favorite/list',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"API Favorite list",
            notes:"API favorite List",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                userModal.findOne({_id: ObjectID(request.query._id)})
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'Favorite List',
                        Data: res.favoriteList
                    })
                })
                .catch(err=>{
                    return reject(err)
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'POST',
        path: '/api/add/to/favorite/list',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"storing data into favorite List",
            notes:"storing data into favorite List",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                var favoriteList = request.payload
                userModal.findOne({_id: ObjectID(request.query._id)}, {favoriteList: { $elemMatch: {_id: request.query.productId}}})
                .then(function(response){
                    if (response.favoriteList.length === 0) {
                        userModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$push: {favoriteList: favoriteList}})
                        .then(function(res){
                            return resolve({
                                status: 'success',
                                message: 'product added successfully in favorite list',
                                data: res
                            })
                        })
                        .catch(err=>{
                            return reject({
                                status: 'error',
                                message: 'Something Went Wrong',
                                error: err
                            })
                        })
                    }else{
                        userModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, { $pull: { 'favoriteList': { _id: request.query.productId } } })
                        .then(function(result){
                            return resolve({
                                status: 'success',
                                message: 'favorite Item Deleted Successfully',
                                data: result
                            })
                        })
                        .catch(err=>{
                            return reject({
                                status: 'error',
                                message: 'Something Went Wrog',
                                error: err
                            })
                        })
                    }
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'POST',
        path: '/api/move/to/beg/list',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"storing data into favorite List",
            notes:"storing data into favorite List",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                const bagItem = request.payload
                userModal.findOne({_id: ObjectID(request.query._id)}, {begItems: { $elemMatch: {_id: request.query.productId}}}, {"begItems.$": 1})
                .then(function(response){
                    if (response.begItems.length === 0) {
                        userModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$push: {begItems: bagItem}})
                        .then(function(response){
                            userModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, { $pull: { 'favoriteList': { _id: request.query.productId } } })
                            .then(function(result){
                                return resolve({
                                    status: 'success',
                                    message: 'Move to beg successfully',
                                    data: result
                                })
                            })
                            .catch(err=>{
                                return reject(err)
                            })
                        })
                        .catch(err=>{
                            return reject(err)
                        })
                    }else{
                        return resolve({
                            status: 'error',
                            message: 'product already in bag',
                        })
                    }
                })
            }
            return new Promise(pr)
        }  
    }
]
export default routes;