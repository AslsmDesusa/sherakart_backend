import { uploadFileToS3 }  from "../../dependencies/uploadImage"
import { DeleteAwsImage }  from "../../dependencies/uploadImage"
import { resolve } from "path";
const componentData = require('../../models/component/componentData')
const ObjectID = require('mongodb').ObjectID

const Joi = require('joi');

require('dotenv').config();

const routes = [
    {
        method: 'POST',
        path: '/api/cover-image-upload',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"Posting cover Image Uplaod",
            notes:"Posting Cover Image Upoload to s3 bucket",
            payload:{
                maxBytes: 1000 * 1000 * 5, // 5 Mb
            }
        },
        handler: async (request, h) => {
            let file =  request.payload.file;
            let pr = async (resolve, reject)=>{
                uploadFileToS3(file, "coverImageSlider", "image",  'sherakart-001')
                .then(({ fileLink }) => {
                    var newCoverImageData = new componentData({
                        //Component Details
                        CompoCateName: request.payload.CompoCateName,
                        productName: request.payload.productName,
                        productPrice: request.payload.productPrice,
                        productDiscount: request.payload.productDiscount,
                        productActualPrice: request.payload.productActualPrice,
                        productDescription: request.payload.productDescription,
                        productCoverImage: fileLink,
                    });
                    newCoverImageData.save()
                    .then(function(response){
                        return resolve({
                            status: 'success',
                            message: 'component data added sucessfully'
                        })
                    })
                    .catch(error=>{
                        return reject(error)
                    })
                });
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'POST',
        path: '/api/update-component-data',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"updateing component data",
            notes:"updateing component data",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                var imageLink = request.payload.oldLink
                let data = imageLink.split('/')
                DeleteAwsImage(data[3], data[4])
                .then(({ Success }) => {
                    let file =  request.payload.file;
                    uploadFileToS3(file, "coverImageSlider", "image",  'sherakart-001')
                    .then(({ fileLink }) => {
                        const data =  {
                            $set:{
                                    CompoCateName: request.payload.CompoCateName,
                                    productName: request.payload.productName,
                                    productPrice: request.payload.productPrice,
                                    productDiscount: request.payload.productDiscount,
                                    productActualPrice: request.payload.productActualPrice,
                                    productDescription: request.payload.productDescription,
                                    productCoverImage: fileLink
                                }
                            }   
                        componentData.findOneAndUpdate({_id: ObjectID(request.query._id)}, data)
                        .then(function(response){
                            return resolve({
                                status: 'success',
                                message: 'component data updated successfully',
                                fileLink: fileLink
                            })  
                        })
                    })
                    .catch(err=>{
                        throw err
                    })    
                })
                .catch(err=>{
                    throw err
                })
            }
            return new Promise(pr)
        }
        
    },
    {
        method: 'GET',
        path: '/api/getting-slider-data',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"Getting Slider Data",
            notes:"Getting Slider Data",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                componentData.find()
                .then(function(response){
                    return resolve(response)
                })
            }
            return new Promise(pr)
        }
        
    },
    {
        method: 'DELETE',
        path: '/api/delete-image/aws', 
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"delete image aws",
            notes:"delete image aws",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                componentData.findOne({_id: ObjectID(request.query._id)})
                .then(function(res){
                    if (res) {
                        var imageLink = res.productCoverImage
                        let data = imageLink.split('/')
                        DeleteAwsImage(data[3], data[4])
                        .then(({ Success }) => {
                            componentData.findOneAndRemove({_id: ObjectID(request.query._id)})
                            .then(function(response){
                                return resolve({
                                    status: 200,
                                    message: 'Data has been delete successfully',
                                    data: response
                                })
                            })
                            .catch(err=>{
                                return reject(err)
                            })
                        })   
                    }else{
                        return resolve({
                            status: 401,
                            mesage: 'Image and Data is not exit into the data base'
                        })
                    }
                })
                .catch(err=>{
                    return reject(err)
                })
            }
            return new Promise(pr)
        }
        
    },
]
export default routes;