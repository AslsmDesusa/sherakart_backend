const colorModal = require('../../models/colorModal/color')
const ObjectID = require('mongodb').ObjectID

const Joi = require('joi');

require('dotenv').config();

const routes = [
    {
        method: 'GET',
        path: '/api/get-color-data',
        config: {
             //include this route in swagger documentation
            description:"getting color data",
            notes:"getting color data",
            tags:['api'],
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                colorModal.find()
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'colors got successfully',
                        data: res
                    })
                })
                .catch(err=>{
                    throw err
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'POST',
        path: '/api/post-color-data',
        config: {
             //include this route in swagger documentation
            description:"positing color data",
            notes:"posting color data",
            tags:['api'],
            validate:{
                payload:{
                    color: Joi.string().required()
                }
            }
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                const newColor = new colorModal({
                    color: request.payload.color
                })
                newColor.save()
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'color saved successfully'
                    })
                })
                .catch(err=>{
                    throw err
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'PUT',
        path: '/api/update-color-data',
        config: {
             //include this route in swagger documentation
            description:"update color data",
            notes:"update color data",
            tags:['api'],
            validate:{
                payload:{
                    color: Joi.string().required()
                }
            }
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                const color = { $set:
                    {
                        color: request.payload.color
                    }
                }
                colorModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, color)
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'color updated successfully'
                    })
                })
                .catch(err=>{
                    throw err
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'DELETE',
        path: '/api/delete-color-data',
        config: {
             //include this route in swagger documentation
            description:"delete color data",
            notes:"delete color data",
            tags:['api'],
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                colorModal.findOneAndRemove({_id: ObjectID(request.query._id)})
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'color deleted successfully'
                    })
                })
                .catch(err=>{
                    throw err
                })
            }
            return new Promise(pr)
        }  
    }
]
export default routes;