const sizeModal = require('../../models/sizeModal/size')
const ObjectID = require('mongodb').ObjectID

const Joi = require('joi');

require('dotenv').config();

const routes = [
    {
        method: 'GET',
        path: '/api/get-size-data',
        config: {
             //include this route in swagger documentation
            description:"getting size data",
            notes:"getting size data",
            tags:['api'],
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                sizeModal.find()
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'sizes got successfully',
                        data: res
                    })
                })
                .catch(err=>{
                    throw err
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'POST',
        path: '/api/post-size-data',
        config: {
             //include this route in swagger documentation
            description:"positing size data",
            notes:"posting size data",
            tags:['api'],
            validate:{
                payload:{
                    wearComponent: Joi.string().required(),
                    size: Joi.string().required()
                }
            }
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                const newSize = new sizeModal({
                    wearComponent: request.payload.wearComponent,
                    size: request.payload.size
                })
                newSize.save()
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'size saved successfully'
                    })
                })
                .catch(err=>{
                    throw err
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'PUT',
        path: '/api/update-size-data',
        config: {
             //include this route in swagger documentation
            description:"update size data",
            notes:"update size data",
            tags:['api'],
            validate:{
                payload:{
                    wearComponent: Joi.string().required(),
                    size: Joi.string().required()
                }
            }
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                const size = { $set:
                    {
                        wearComponent: request.payload.wearComponent,
                        size: request.payload.size
                    }
                }
                sizeModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, size)
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'size updated successfully'
                    })
                })
                .catch(err=>{
                    throw err
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'DELETE',
        path: '/api/delete-size-data',
        config: {
             //include this route in swagger documentation
            description:"delete size data",
            notes:"delete size data",
            tags:['api'],
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                sizeModal.findOneAndRemove({_id: ObjectID(request.query._id)})
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'size deleted successfully'
                    })
                })
                .catch(err=>{
                    throw err
                })
            }
            return new Promise(pr)
        }  
    }
]
export default routes;