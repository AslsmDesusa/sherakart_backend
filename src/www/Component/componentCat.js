const CatCompo = require('../../models/component/componentCat');
const ObjectID = require('mongodb').ObjectID

const Joi = require('joi');

require('dotenv').config();

const routes = [
    {
        method: 'POST',
        path: '/api/add/compo',
        config: {
            description: 'API Add Compo',
            notes: 'Add Compo',
            tags: ['api'],
            validate: {
                payload:{
                    CompoCateName: Joi.string()
                }
            }
        },
        handler: async (request, h) =>{
            let pr = async (resolve, reject) => {
                const newCompo = new CatCompo({
                    "CompoCateName": request.payload.CompoCateName
                });
                newCompo.save()
                .then(function(res){
                    return resolve({
                        status: 200,
                        message: 'component successfully added',
                    })
                })
                .catch(err=>{
                    return reject(err)
                })
            }
            return new Promise(pr)
        }
    },
    {
        method: 'GET',
        path: '/api/compo/cat',
        config: {
            description: 'API Add Compo',
            notes: 'Add Compo',
            tags: ['api']
        },
        handler: async (request, h) =>{
            let pr = async (resolve, reject) => {
                CatCompo.find()
                .then(function(res){
                    return resolve({
                        status: 200,
                        message: 'success',
                        data: res
                    })
                })
                .catch(err=>{
                    return reject(err)
                })
            }
            return new Promise(pr)
        }
    }
]
export default routes;