import { uploadFileToS3 }  from "../../dependencies/uploadImage"
import { DeleteAwsImage }  from "../../dependencies/uploadImage"
import { resolve } from "path";
const ProductData = require('../../models/productModal/Product')
const order = require('../../models/orderModal/orderModal')
const ObjectID = require('mongodb').ObjectID

const Joi = require('joi');

require('dotenv').config();

const routes = [
    // searchbar Api
    {
        method: 'GET',
        path: '/api/search',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"product search",
            notes:"user can search products",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                var query = {productName:{$regex: request.query.product, $options: 'i'}}
                ProductData.find(query).count()
                .then(function(count){
                    ProductData.find(query).limit(20).skip(20 * request.query.count)
                    .then(function(res){
                        return resolve({
                            status: 'success',
                            message: 'products get successfully',
                            data: res,
                            count: count
                        })
                    })
                    .catch(err=>{
                        throw err
                    })
                })
                .catch(err=>{
                    throw err
                })
            }
            return new Promise(pr)
        }  
    },
    // // searchbar Api
    {
        method: 'GET',
        path: '/api/filter-by-gender',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"product search",
            notes:"user can search products",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                ProductData.find({productGender: request.query.gender, typeOfWear: request.query.typeOfWear, productCat: request.query.productCat}).limit(20).skip(20 * request.query.count).count()
                .then(function(count){
                    ProductData.find({productGender: request.query.gender, typeOfWear: request.query.typeOfWear, productCat: request.query.productCat}).limit(20).skip(20 * request.query.count)
                    .then(function(res){
                        return resolve({
                            status: 'success',
                            message: 'products get successfully',
                            data: res,
                            count: count
                        })
                    })
                    .catch(err=>{
                        throw err
                    })
                })
            }
            return new Promise(pr)
        }  
    },
    // // get orders
    {
        method: 'GET',
        path: '/api/get-product-orders',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"product orders",
            notes:"product orders",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                var query = { $or : [ { 'status' : 'Customer Order' }, { 'status' : 'Factory Send Out' }, { 'status' : 'Deliver To Customer' } ] }
                order.find(query)
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'orders get successfully',
                        data: res
                    })
                })
            }
            return new Promise(pr)
        }  
    },
    // update status
    {
        method: 'PUT',
        path: '/api/update-product-status',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"product orders",
            notes:"product orders",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                const updateStatus = {$set:{
                    status: request.payload.status
                }}
                order.findOneAndUpdate({_id: ObjectID(request.query._id)}, updateStatus)
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'Status updated successfully',
                    })
                })
            }
            return new Promise(pr)
        }  
    },
    // get count
    {
        method: 'GET',
        path: '/api/get-product-count',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"product count",
            notes:"product count",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                ProductData.find().count()
                .then(function(res){
                    var query = { $or : [ { 'status' : 'Customer Order' }, { 'status' : 'Factory Send Out' }, { 'status' : 'Deliver To Customer' } ] }
                    order.find(query).count()
                    .then(function(count){
                        return resolve({
                            status: 'success',
                            message: 'counts get successfully',
                            products: res,
                            order: count
                        })
                    })
                })
            }
            return new Promise(pr)
        }  
    },
    // get count
    {
        method: 'GET',
        path: '/api/get-all-complete-orders',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"product count",
            notes:"product count",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                order.find({ 'status' : 'Customer Received' })
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'get data successfully',
                        products: res
                    })
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'POST',
        path: '/api/upload-product/image',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"Posting product Image with there data",
            notes:"Posting Image with data Upoload to s3 bucket",
            payload:{
                maxBytes: 1000 * 1000 * 5, // 5 Mb
            }
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                var file = request.payload.file
                uploadFileToS3(file, "ProductImages", "image",  'sherakart-001')
                .then(({ fileLink })=>{
                    return resolve(fileLink)
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'GET',
        path: '/api/product',
        config: {
            description: 'API get products',
            notes: 'products',
            tags: ['api'],
            validate: {
                query: {
                    _id: Joi.string().required()
                }
            } 
        },
        handler: async (request, h) =>{
            let pr = async (resolve, reject) => {
                ProductData.find({_id: ObjectID(request.query._id)})
                .then(function(res){
                    return resolve({
                        status: 200,
                        message: 'success',
                        data: res
                    })
                })
                .catch(err=>{
                    return reject({
                        status: 400,
                        message: 'error',
                        err: err
                    })
                })
            }
            return new Promise(pr)
        }
    },
    {
        method: 'GET',
        path: '/api/products/{productCat}',
        config: {
            description: 'API get products',
            notes: 'products',
            tags: ['api'],
            validate: {
                params: {
                    productCat: Joi.string().required(),
                }
            } 
        },
        handler: async (request, h) =>{
            let pr = async (resolve, reject) => {
                var query = {productCat:{$regex: request.params.productCat, $options: 'i'}}
                ProductData.find(query).count()
                .then(function(count){
                    ProductData.find(query).limit(20).skip(20 * request.query.count)
                    .then(function(res){
                        return resolve({
                            status: 200,
                            message: 'success',
                            data: res,
                            count: count
                        })
                    })
                    .catch(err=>{
                        return reject(err)
                    })
                })
                .catch(err=>{
                    return reject(err)
                })
            }
            return new Promise(pr)
        }
    },
    {
        method: 'GET',
        path: '/api/products',
        config: {
            description: 'API get products',
            notes: 'products',
            tags: ['api']
        },
        handler: async (request, h) =>{
            let pr = async (resolve, reject) => {
                ProductData.find().limit(20).skip(20 * request.query.count)
                .then(function(res){
                    return resolve({
                        status: 200,
                        message: 'success',
                        data: res
                    })
                })
                .catch(err=>{
                    return reject(err)
                })
            }
            return new Promise(pr)
        }
    },
    {
        method: 'POST',
        path: '/api/upload-product/data',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"Posting product Image with there data",
            notes:"Posting Image with data Upoload to s3 bucket",
            payload:{
                maxBytes: 1000 * 1000 * 5, // 5 Mb
            }
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                var newProductData = new ProductData({
                    //Component Details
                    typeOfWear: request.payload.typeOfWear,
                    productCat: request.payload.productCat,
                    productName: request.payload.productName,
                    productPrice: request.payload.productPrice,
                    productDiscount: request.payload.productDiscount,
                    productActualPrice: request.payload.productActualPrice,
                    productColor: request.payload.productColor,
                    productGender: request.payload.productGender,
                    productDescription: request.payload.productDescription,
                    productSizes: request.payload.productSizes,
                    productQuntity: request.payload.productQuntity,
                    productImagesLength: request.payload.productImagesLength,
                    productRelativeImages: request.payload.productRelativeImages
                });
                newProductData.save()
                .then(function(res){
                    return resolve({
                        status: 200,
                        message: 'success',
                        data: res
                    })
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'GET',
        path: '/api/delete-product/aws',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"delete product aws",
            notes:"delete product with aws images",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                componentData.findOne({_id: ObjectID(request.query._id)})
                .then(function(res){
                    if (res) {
                        var imageLink = res.productCoverImage
                        let data = imageLink.split('/')
                        DeleteAwsImage(data[3], data[4])
                        .then(({ Success }) => {
                            componentData.findOneAndRemove({_id: ObjectID(request.query._id)})
                            .then(function(response){
                                return resolve({
                                    status: 200,
                                    message: 'Data has been deleted successfully',
                                    data: response
                                })
                            })
                            .catch(err=>{
                                return reject(err)
                            })
                        })   
                    }else{
                        return resolve({
                            status: 401,
                            mesage: 'Image and Data is not exit into the data base'
                        })
                    }
                })
                .catch(err=>{
                    return reject(err)
                })
            }
            return new Promise(pr)
        }
        
    },
    {
        method: 'PUT',
        path: '/api/update-product/aws',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"update product data",
            notes:"update product data",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                const updateData = {$set:{
                    typeOfWear: request.payload.typeOfWear,
                    productCat: request.payload.productCat,
                    productName: request.payload.productName,
                    productPrice: request.payload.productPrice,
                    productDiscount: request.payload.productDiscount,
                    productActualPrice: request.payload.productActualPrice, 
                    productGender: request.payload.productGender,
                    productDescription: request.payload.productDescription,
                    productColor: request.payload.productColor,
                    productSizes: request.payload.productSizes,
                    productQuntity: request.payload.productQuntity,
                    productRelativeImages: request.payload.productRelativeImages,
                }}
                ProductData.findOneAndUpdate({_id: ObjectID(request.query._id)}, updateDate)
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'data updated successfully'
                    })
                })
                .catch(err=>{
                    throw err
                })
            }
            return new Promise(pr)
        }
        
    },
    {
        method: 'DELETE',
        path: '/api/delete-product-data',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"delete product aws",
            notes:"delete product with aws images",
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                ProductData.findOne({_id: ObjectID(request.query._id)})
                .then(function(res){
                    if (res) {
                        for (let index = 0; index < res.productRelativeImages.length; index++) {
                            // Get num of each fruit
                            var imageLink = res.productRelativeImages[index]
                            let data = imageLink.split('/')
                            DeleteAwsImage(data[3], data[4])
                            .then(({ Success }) => {
                                console.log(Success)
                            })
                        }
                        ProductData.findOneAndRemove({_id: ObjectID(request.query._id)})
                        .then(function(response){
                            return resolve({
                                status: 200,
                                message: 'Data has been deleted successfully',
                            })
                        })
                        .catch(err=>{
                            return reject(err)
                        })
                    }else{
                        return resolve({
                            status: 401,
                            mesage: 'Image and Data is not exit into the data base'
                        })
                    }
                })
                .catch(err=>{
                    return reject(err)
                })
            }
            return new Promise(pr)
        }
        
    },
]
export default routes;