const productCat = require('../../models/productModal/productCat');
const ObjectID = require('mongodb').ObjectID

const Joi = require('joi');

require('dotenv').config();

const routes = [
    {
        method: 'POST',
        path: '/api/add/procduct-category',
        config: {
            description: 'API Add product category',
            notes: 'Add add product category',
            tags: ['api'],
            validate: {
                payload:{
                    productType: Joi.string(),
                    productCateName: Joi.string()
                }
            }
        },
        handler: async (request, h) =>{
            let pr = async (resolve, reject) => {
                const newProCat = new productCat({
                    "productType": request.payload.productType,
                    "productCateName": request.payload.productCateName
                });
                newProCat.save()
                .then(function(res){
                    return resolve({
                        status: 200,
                        message: 'success',
                        text: 'Product Category successfully added'
                    })
                })
                .catch(err=>{
                    return reject(err)
                })
            }
            return new Promise(pr)
        }
    },
    {
        method: 'GET',
        path: '/api/product-cat',
        config: {
            description: 'API Add Compo',
            notes: 'Add Compo',
            tags: ['api']
        },
        handler: async (request, h) =>{
            let pr = async (resolve, reject) => {
                productCat.find()
                .then(function(res){
                    return resolve({
                        status: 200,
                        message: 'success',
                        data: res
                    })
                })
                .catch(err=>{
                    return reject(err)
                })
            }
            return new Promise(pr)
        }
    },
    {
        method: 'GET',
        path: '/api/product-cat/delete',
        config: {
            description: 'API Delete',
            notes: 'Delete product',
            tags: ['api']
        },
        handler: async (request, h) =>{
            let pr = async (resolve, reject) => {
                productCat.findOneAndRemove({_id: ObjectID(request.query._id)})
                .then(function(res){
                    return resolve({
                        status: 200,
                        message: 'success',
                    })
                })
                .catch(err=>{
                    return reject(err)
                })
            }
            return new Promise(pr)
        }
    },
    {
        method: 'PUT',
        path: '/api/update/product-cat',
        config: {
            description: 'API update',
            notes: 'update product',
            tags: ['api']
        },
        handler: async (request, h) =>{
            let pr = async (resolve, reject) => {
                productCat.findOneAndUpdate({_id: ObjectID(request.query._id)}, request.payload)
                .then(function(res){
                    return resolve({
                        status: 200,
                        message: 'product category updated successfully',
                    })
                })
                .catch(err=>{
                    return reject(err)
                })
            }
            return new Promise(pr)
        }
    }
]
export default routes;