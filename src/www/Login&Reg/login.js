const UserModel = require('../../models/userModal/userModal');
const ObjectID = require('mongodb').ObjectID


const {sent_text} = require('../../dependencies/SendText');
const {WelcomeMSG} = require('../../dependencies/SendText');


const Joi = require('joi');

require('dotenv').config();

const routes = [
    {
        method: 'GET',
        path: '/api/get-all-users',
        config: {
        // include this route in swagger documentation
            description:"Home Route",
            notes:"Home Page is comming soon",
            tags:['api'],
        },
        handler: async(request, h)=>{
            let pr = async (resolve, reject) => {
                UserModel.find().limit(20).skip(20 * request.query.count)
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'get all users',
                        data: res
                    })
                })
            }
            return new Promise(pr)
        }
    },
    {
        method: 'GET',
        path: '/api/search-users',
        config: {
        // include this route in swagger documentation
            description:"Home Route",
            notes:"Home Page is comming soon",
            tags:['api'],
        },
        handler: async(request, h)=>{
            let pr = async (resolve, reject) => {
                var query = { $or : [ {'userDetails.firstName':{$regex: request.query.firstName, $options: 'i'}}, {'privateDetails.email':{$regex: request.query.email, $options: 'i'}}, {'privateDetails.phoneNumber':{$regex: request.query.phoneNumber, $options: 'i'}}]}
                UserModel.find(query).limit(20).skip(20 * request.query.count)
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'search result',
                        data: res
                    })
                })
            }
            return new Promise(pr)
        }
    },
    {
        method: 'DELETE',
        path: '/api/get-delete-user',
        config: {
        // include this route in swagger documentation
            description:"Home Route",
            notes:"Home Page is comming soon",
            tags:['api'],
        },
        handler: async(request, h)=>{
            let pr = async (resolve, reject) => {
                UserModel.findOneAndDelete({_id: ObjectID(request.query._id)})
                .then(function(res){
                    return resolve({
                        status: 'success',
                        message: 'User Deleted successfully'
                    })
                })
                .catch(err=>{
                    throw err
                })
            }
            return new Promise(pr)
        }
    },
    {
        method: 'GET',
        path: '/api/verification/otp/{_id}/{otp}',
        config: {
            description: 'API moderators',
            notes: 'Getting user otp api',
            tags: ['api'],
            validate: {
                params: {
                    _id: Joi.string().required(),
                    otp: Joi.string().required(),
                }
            }
        },
        handler: async (request, h) =>{
            const date = new Date().toISOString();
            var newContectInfo = { $set: 
                {
                    otp: 'verified',
                    expireAt: '',
                    whyNotValid: '',
                    isValidUser: true,
                    createAt: date
                }
            };
            let pr = async (resolve, reject) => {
                UserModel.findOne({_id: ObjectID(request.params._id)})
                .then(function(response){
                    if (!response) {
                        return resolve({"StatusCode": 200, "status": "error", "message": 'Expired'})
                    }else if (response.otp == request.params.otp) {
                        UserModel.findOneAndUpdate({_id: ObjectID(request.params._id)},newContectInfo)
                        .then(function(data){
                            let message = `Hi ${data.userDetails.firstName } ${data.userDetails.lastName } your account successfully verified Thanx To Be A Member of Sherakart.`
                            WelcomeMSG(data.privateDetails.phoneNumber, message)
                            .then(({ Message }) => {
                                return resolve({"StatusCode": 200,"status": "success", "message": Message})
                            })
                        })
                    } else {
                        return resolve({"StatusCode": 400,"status": "error", "message": "Wrong OTP"})
                    }
                })
            }
            return new Promise(pr)
        }
    },
    {
        method: 'POST',
        path: '/api/create/users',
        config: {
            description: 'Creating user with bcrypt password',
            notes: 'user can signup',
            tags: ['api'],
            validate: {
                payload: {
                    firstName:Joi.string().required(),
                    lastName:Joi.string().required(),
                    phoneNumber:Joi.string().required(),
                    email:Joi.string().required(),
                    password: Joi.string().required(),
                    gender: Joi.string().required(),
                    role: Joi.string()
                }
            }
        },
        handler: async (request, h) => {
            const newUser = new UserModel({
                userDetails:{
                    firstName:request.payload.firstName,
                    lastName:request.payload.lastName,
                    gender: request.payload.gender
                },
                privateDetails:{
                    phoneNumber: request.payload.phoneNumber,
                    email: request.payload.email,
                    password: request.payload.password,
                },
                role: request.payload.role,
                isValidUser: false,
                whyNotValid: 'You Are Not Valid User Please verify Your Phone Number',
                isLogin: false,
                otp: ''
            }); 
            let pr = async (resolve, reject) => {
                var query = { $or : [ { 'privateDetails.phoneNumber' : request.payload.phoneNumber }, { 'privateDetails.email' : request.payload.email } ] }
                UserModel.findOne(query ,async function (err, data) {
                    if (err) {
                        return resolve({"StatusCode": 400, "status": "error", "message": err})
                    } else if (!data) {
                        sent_text(request.payload.firstName, request.payload.lastName, request.payload.phoneNumber)
                        .then(({ Message, OTP }) => {
                            newUser.otp = OTP
                            newUser.save()
                            .then(function(res){
                                return resolve({"StatusCode": 200,"status": 'success',"message": Message,"data": res})
                            })
                        })
                    } else if (data.isValidUser == false) {
                        UserModel.findOneAndRemove({_id: ObjectID(data._id)})
                        .then(function(response){
                            sent_text(request.payload.firstName, request.payload.lastName, request.payload.phoneNumber)
                            .then(({ Message, OTP }) => {
                                newUser.otp = OTP
                                newUser.save()
                                .then(function(res){
                                    return resolve({"StatusCode": 200,"status": 'success',"message": Message,"data": res
                                    })
                                })
                                .catch(error => {
                                    throw error;
                                })
                            })  
                        })
                    }else if (data.privateDetails.phoneNumber == request.payload.phoneNumber) {
                        return resolve({
                            "status": "error", "message": `This phone number ${request.payload.phoneNumber} already exist`})
                        }else if (data.privateDetails.email == request.payload.email) {
                            return resolve({"status": "error", "message": `This email address ${request.payload.email} already exist`})
                        }else{
                            return resolve({"status": "error", "message": `User already exist`})
                        }
                });
            }
            return new Promise(pr)

        }
    },
    {
        method: 'POST',
        path: '/api/create/admin',
        config: {
            description: 'Creating admin with bcrypt password',
            notes: 'admin can create more admins',
            tags: ['api'],
            validate: {
                payload: {
                    firstName:Joi.string().required(),
                    lastName:Joi.string().required(),
                    phoneNumber:Joi.string().required(),
                    email:Joi.string().required(),
                    password: Joi.string().required(),
                    gender: Joi.string().required()
                }
            }
        },
        handler: async (request, h) => {
            const newUser = new UserModel({
                userDetails:{
                    firstName:request.payload.firstName,
                    lastName:request.payload.lastName,
                    gender: request.payload.gender
                },
                privateDetails:{
                    phoneNumber: request.payload.phoneNumber,
                    email: request.payload.email,
                    password: request.payload.password,
                },
                role: 'Admin',
                isValidUser: true,
                whyNotValid: 'created by admin',
                isLogin: false,
                otp: '',
                expireAt: ''
            }); 
            let pr = async (resolve, reject) => {
                var query = { $or : [ { 'privateDetails.phoneNumber' : request.payload.phoneNumber }, { 'privateDetails.email' : request.payload.email } ] }
                UserModel.findOne(query ,async function (err, data) {
                    if (err) {
                        return resolve({"StatusCode": 400, "status": "error", "message": err})
                    } else if (!data) {
                        newUser.save()
                        .then(function(res){
                            return resolve({
                                "StatusCode": 200,
                                "status": 'success',
                                "message": 'Admin Created successfully',
                            })
                        })
                        .catch(err=>{
                            throw err
                        })
                    }else if (data.privateDetails.phoneNumber == request.payload.phoneNumber) {
                        return resolve({
                            "status": "error", "message": `This phone number ${request.payload.phoneNumber} already exist`})
                        }else if (data.privateDetails.email == request.payload.email) {
                            return resolve({"status": "error", "message": `This email address ${request.payload.email} already exist`})
                        }else{
                            return resolve({"status": "error", "message": `User already exist`})
                        }
                });
            }
            return new Promise(pr)

        }
    },
    {
        method: 'POST',
        path: '/api/login',
        config: {
            description: 'login authentication done by JWT',
            notes: 'user can login',
            tags: ['api'],
            validate: {
                payload: {
                    username: Joi.string().required(),
                    password: Joi.string().required()
                }
            }
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject) => {
                var query = { $or : [ { 'privateDetails.phoneNumber' : request.payload.username }, { 'privateDetails.email' : request.payload.username } ] }
                UserModel.find(query ,async function (err, data) {
                    if (err) {
                        return resolve({"status": "error", "message": err})
                    } else if (data.length == 0) {
                        return resolve({"status": "error", "message": "User not exist"})
                    }else if (data[0]['isValidUser'] == false) {
                        return resolve({"status": "error", "message": data[0]['whyNotValid']})
                    }else {
                        const validUser = await UserModel.login(request.payload.username, request.payload.password);
                        if (validUser) {
                            const token = UserModel.generateToken(data[0])
                            return resolve({
                                status: 'success',
                                message: token,
                                userid: data[0]._id,
                            });
                        } else {
                            return resolve({"status": "error", "message": "Invalid password"})
                        }
                    }
                });
            }
            return new Promise(pr)

        }
    },
    {
		method: 'PUT',
		path: '/api/change/password/{_id}/{username}',
		config: {
			description: 'API Change password',
			notes: 'Change password api',
			tags: ['api'],
			validate:{
				params:{
					_id: Joi.string().required(),
					username: Joi.string().required(),
				},
				payload:{
					currentPassword: Joi.string().required(),
					password: Joi.string().required()
				}
			}
		},
		handler: async (request, h) =>{
            let db = async (resolve, reject) => {
				const validUser = await UserModel.login(request.params.username, request.payload.currentPassword);
				if (validUser) {
					UserModel.findOne({'_id': ObjectID(request.params._id)})
					.then(function(res){
                        const updatePassword = new UserModel({
                            userDetails:{
                                firstName:res.userDetails.firstName,
                                lastName:res.userDetails.lastName,
                                gender: res.userDetails.gender
                            },
                            privateDetails:{
                                phoneNumber: res.privateDetails.phoneNumber,
                                email: res.privateDetails.email,
                                password: request.payload.password,
                            },
                            buyerAddress: res.buyerAddress,
                            begItems: res.begItems,
                            favoriteList: res.favoriteList,
                            orderList: res.orderList,
                            role: res.role,
                            isValidUser: res.isValidUser,
                            whyNotValid: res.whyNotValid,
                            isLogin: res.isLogin,
                            otp: res.otp,
                            expireAt: res.expireAt,
                            createAt: res.createAt,
                        })
						UserModel.findOneAndRemove({_id: request.params._id})
						.then(function(response){
							updatePassword.save(function async(err, data){
								return resolve({
									status: 'success',
									message: 'Password successfully Updated',
								})
							})
						})
					})
					.catch((err)=>{
						return reject(err)
					})
				}else{
					return resolve({"status": "error", "message": "Invalid password"})
				}
            }
            return new Promise(db)
		}
    },
    // send otp for change password
    {
		method: 'GET',
		path: '/api/get/otp/change/password/{username}',
		config: {
			description: 'API Change password',
			notes: 'Change password api',
            tags: ['api'],
            validate:{
				params:{
					username: Joi.string().required(),
				}
			}
		},
		handler: async (request, h) =>{
            let db = async (resolve, reject) => {
                UserModel.findOne({'privateDetails.phoneNumber': request.params.username})
                .then(res=>{
                    if (res) {
                        sent_text(res.userDetails.firstName, res.userDetails.lastName, request.params.username)
                        .then(({ Message, OTP })=> {
                            var newContectInfo = { $set: 
                                {
                                    otp: OTP
                                }
                            }
                            UserModel.findOneAndUpdate({'privateDetails.phoneNumber': request.params.username }, newContectInfo)
                            .then(res=>{  
                                return resolve({"status": "success", "message": Message})
                            })    
                        })
                    }else{
                        return resolve({"status": "error", "message": "Invalid User"})
                    }
                })
            }
            return new Promise(db)
		}
    },
    // verify otp
    {
		method: 'POST',
		path: '/api/verify/otp/{username}',
		config: {
			description: 'API Change password',
			notes: 'Change password api',
			tags: ['api'],
			validate:{
				params:{
                    username: Joi.string().required()
				},
				payload:{
                    otp: Joi.string().required(),
				}
			}
		},
		handler: async (request, h) =>{
            let db = async (resolve, reject) => {
                    UserModel.findOne({'privateDetails.phoneNumber': request.params.username, otp: request.payload.otp})
                    .then(res=>{
                        if (res) {
                            return resolve({
                                status: 'success',
                                message: 'otp has been matched'
                            })
                        }else{
                            return resolve({
                                status: 'error',
                                message: 'the otp entered is incorrect'
                            })
                        }
                    }).catch(err=>{
                        throw err
                    })
            }
            return new Promise(db)
		}
    },
    // forget password
    {
		method: 'PUT',
		path: '/api/change/password/{username}',
		config: {
			description: 'API Change password',
			notes: 'Change password api',
			tags: ['api'],
			validate:{
				params:{
                    username: Joi.string().required()
				},
				payload:{
                    password: Joi.string().required(),
                    otp: Joi.string().required(),
				}
			}
		},
		handler: async (request, h) =>{
            let db = async (resolve, reject) => {
					UserModel.findOne({'privateDetails.phoneNumber': request.params.username, otp: request.payload.otp})
					.then(function(res){
                        if (res) {
                            const updatePassword = new UserModel({
                                userDetails:{
                                    firstName:res.userDetails.firstName,
                                    lastName:res.userDetails.lastName,
                                    gender: res.userDetails.gender
                                },
                                privateDetails:{
                                    phoneNumber: res.privateDetails.phoneNumber,
                                    email: res.privateDetails.email,
                                    password: request.payload.password,
                                },
                                buyerAddress: res.buyerAddress,
                                begItems: res.begItems,
                                favoriteList: res.favoriteList,
                                orderList: res.orderList,
                                role: res.role,
                                isValidUser: res.isValidUser,
                                whyNotValid: res.whyNotValid,
                                isLogin: res.isLogin,
                                otp: res.otp,
                                expireAt: res.expireAt,
                                createAt: res.createAt,
                            })
                            UserModel.findOneAndRemove({'privateDetails.phoneNumber': request.params.username})
                            .then(function(response){
                                updatePassword.save(function async(err, data){
                                    return resolve({
                                        status: 'success',
                                        message: 'Password successfully Updated',
                                    })
                                })
                            })
                            .catch((err)=>{
                                return reject(err)
                            })
                        }else{
                            return resolve({"status": "error", "message": "Invalid OTP"})
                        }
                    })
                    .catch((err)=>{
                        return reject(err)
                    })
            }
            return new Promise(db)
		}
    },
]
export default routes;