const UserModel = require('../../models/userModal/userModal');
const ObjectID = require('mongodb').ObjectID

const {sent_text} = require('../../dependencies/SendText');
const Joi = require('joi');

require('dotenv').config();

const routes = [
    {
        method: 'POST',
        path: '/api/edit/personal/info/{_id}',
        config: {
            description: 'API Edit User Data',
            notes: 'Edit Personal Info',
            tags: ['api'],
            validate: {
                params: {
                    _id: Joi.string().required(),
                },
                payload:{
                    firstName: Joi.string(),
                    lastName: Joi.string(),
                    gender: Joi.string(),
                }
            }
        },
        handler: async (request, h) =>{
            var newContectInfo = { $set: 
                {
                    userDetails:{
                        firstName: request.payload.firstName,
                        lastName: request.payload.lastName,
                        gender: request.payload.gender                    }
                }
            };
            let pr = async (resolve, reject) => {
                UserModel.findOneAndUpdate({_id: ObjectID(request.params._id)}, newContectInfo)
                .then(function(success){
                    UserModel.find({_id: ObjectID(request.params._id)})
                    .then(function(response){
                        const token = UserModel.generateToken(response[0])
                        return resolve({
                            "status": "success", 
                            "message": 'Your personal information successfully updated',
                            "token": token
                        })
                    })
                })
                .catch(error=>{
                    throw error
                })
            }
            return new Promise(pr)
        }
    },
    {
        method: 'POST',
        path: '/api/update/phone/{_id}',
        config: {
            description: 'API Edit User Data',
            notes: 'Edit Personal Info',
            tags: ['api'],
            validate: {
                params: {
                    _id: Joi.string().required(),
                },
                payload: {
                    phoneNumber: Joi.string().required(),
                    otp: Joi.string().required()
                }
            }
        },
        handler: async (request, h) =>{
            let pr = async (resolve, reject) => {
                UserModel.findOne({_id: ObjectID(request.params._id)})
                .then(function(response){
                    if (response) {
                        const newPhone = { $set:
                            {
                                privateDetails: {
                                    email: response.privateDetails.email,
                                    password: response.privateDetails.password,
                                    phoneNumber: request.payload.phoneNumber,
                                }
                            }
                        }
                        if (response.otp == request.payload.otp) {
                            UserModel.findOneAndUpdate({_id: ObjectID(request.params._id)}, newPhone)
                            .then(function(res){
                                UserModel.find({_id: ObjectID(request.params._id)})
                                .then(function(response){
                                    const token = UserModel.generateToken(response[0])
                                    return resolve({
                                        "status": "success", 
                                        "message": "Phone Number update successfully",
                                        "token": token
                                    })
                                })
                                .catch(error=>{
                                    throw error
                                })
                            })
                            .catch(err=>{
                                return resolve({
                                    status: 'error',
                                    message: 'Something Went Wrong',
                                    error: err
                                })
                            })
                        }else{
                            return resolve({
                                status: 'error',
                                message: 'Wrong OTP',
                            })
                        }
                    }else{
                        return resolve({
                            status: 'error',
                            message: 'Something Went Wrong'
                        })
                    }                    
                })
                .catch(error=>{
                    throw error
                })
            }
            return new Promise(pr)
        }
    },
    {
        method: 'POST',
        path: '/api/get/otp/to-change/phone/{_id}',
        config: {
            description: 'API Edit User Data',
            notes: 'Edit Personal Info',
            tags: ['api'],
            validate: {
                params: {
                    _id: Joi.string().required(),
                },
                payload: {
                    username: Joi.string().required(),
                    password: Joi.string().required(),
                    newNumber: Joi.string().required()
                }
            }
        },
        handler: async (request, h) =>{
            const otpForUpdatePassword = { $set:
                {
                    otp: ''
                }
            }
            let pr = async (resolve, reject) => {
                UserModel.findOne({'privateDetails.phoneNumber': request.payload.newNumber}, async function (err, data){
                    if (data) {
                        return resolve({
                            status: 'error',
                            message: 'This Phone Number already in use',
                        })
                    }else{
                        const validUser = await UserModel.login(request.payload.username, request.payload.password);
                        if (validUser) {
                            UserModel.findOne({_id: ObjectID(request.params._id)})
                            .then(function(response){
                                sent_text(response.userDetails.firstName, response.userDetails.lastName, response.privateDetails.phoneNumber)
                                    .then(({ Message, OTP }) => {
                                        otpForUpdatePassword.otp = OTP
                                        UserModel.findOneAndUpdate({_id: ObjectID(request.params._id)}, otpForUpdatePassword)
                                        .then(function(res){
                                            return resolve({
                                                status: 'success',
                                                message: 'OTP has been sent to your old number',
                                                date: res
                                            })
                                        })
                                        .catch(err=>{
                                            throw err
                                        })
                                    })
                            })
                            .catch(error=>{
                                throw error
                            })
                        }else{
                            return resolve({
                                status: 'error',
                                message: 'wrong password',
                            })
                        }
                    }
                })
            }
            return new Promise(pr)
        }
    },
    {
        method: 'GET',
        path: '/api/refresh/token/{_id}',
        config: {
            description: 'API Edit User Data',
            notes: 'Edit Personal Info',
            tags: ['api'],
            validate: {
                params: {
                    _id: Joi.string().required(),
                },
            }
        },
        handler: async (request, h) =>{
            let pr = async (resolve, reject) => {
                UserModel.find({_id: ObjectID(request.params._id)})
                .then(function(response){
                    const token = UserModel.generateToken(response[0])
                    return resolve({
                        "status": "success", 
                        "message": 'refresh token',
                        "token": token
                    })
                })
                .catch(error=>{
                    throw error
                })
            }
            return new Promise(pr)
        }
    }
]
export default routes;