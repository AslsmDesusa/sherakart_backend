const UserModel = require('../../models/userModal/userModal');
const addressModal = require('../../models/userModal/addressModal');
const ObjectID = require('mongodb').ObjectID

const Joi = require('joi');

require('dotenv').config();

const routes = [
    {
        method: 'POST',
        path: '/api/add/address',
        config: {
        // include this route in swagger documentation
            description:"edit address",
            notes:"update address",
            tags:['api'],
        },
        handler: async(request, h)=>{
            let pr = async (resolve, reject) => {
                const newAddress = new addressModal(request.payload)
                UserModel.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$push: {buyerAddress: newAddress}})
                .then(function(response){
                    return resolve({
                        status:'success',
                        message: 'New Address save successfully',
                        data: response
                    })
                })
                .catch(err=>{
                    return reject(err)
                })
            }
            return new Promise(pr)
        }
    },
    {
        method: 'GET',
        path: '/api/delete/address',
        config: {
        // include this route in swagger documentation
            description:"delete address",
            notes:"delete address",
            tags:['api'],
        },
        handler: async(request, h)=>{
            let pr = async (resolve, reject) => {
                UserModel.findOneAndUpdate({_id: ObjectID(request.query._id)}, { $pull: { 'buyerAddress': { _id: ObjectID(request.query.addressID) } } })
                .then(function(response){
                    if (response) {
                        return resolve({
                            status:'success',
                            message: 'Address deleted successfully',
                        })   
                    }else{
                        return resolve({
                            status:'success',
                            message: 'Id was not found',
                        })
                    }
                })
                .catch(err=>{
                    return reject(err)
                })
            }
            return new Promise(pr)
        }
    },
    {
        method: 'POST',
        path: '/api/update/address',
        config: {
        // include this route in swagger documentation
            description:"edit address",
            notes:"update address",
            tags:['api'],
        },
        handler: async(request, h)=>{
            let pr = async (resolve, reject) => {
                const newAddress = new addressModal(request.payload)
                UserModel.findOneAndUpdate({_id: ObjectID(request.query._id)}, { $pull: { 'buyerAddress': { _id: ObjectID(request.query.addressID) } } })
                .then(function(response){
                    UserModel.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$push: {buyerAddress: newAddress}})
                    .then(function(result){
                        return resolve({
                            status:'success',
                            message: 'Address updated successfully',
                        })
                    })
                    .catch(err=>{
                        return resolve(err)
                    })
                })
                .catch(err=>{
                    return reject(err)
                })
            }
            return new Promise(pr)
        }
    },
    {
        method: 'GET',
        path: '/api/get/address',
        config: {
        // include this route in swagger documentation
            description:"getting address",
            notes:"get address",
            tags:['api'],
        },
        handler: async(request, h)=>{
            let pr = async (resolve, reject) => {
                const buyerAddress = request.payload
                UserModel.findOne({_id: ObjectID(request.query._id)})
                .then(function(response){
                    return resolve({
                        status:'success',
                        message: 'Address Item',
                        data: response.buyerAddress
                    })
                })
                .catch(err=>{
                    return reject(err)
                })
            }
            return new Promise(pr)
        }
    },
]
export default routes;