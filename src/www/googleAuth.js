import axios from 'axios';
const UserModel = require('../models/userModal/userModal')

const {google} = require('googleapis');

const oauth2Client = new google.auth.OAuth2(
	"286127493898-uv1bkfuth620ec9rmdlrp4p8h51d89i3.apps.googleusercontent.com",
	"QeOYGU8HJ7F2HW055MPgsA51",
	"http://localhost:8000/oauthcallback"
);


// https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=



// generate a url that asks permissions for Google+ and Google Calendar scopes
const scopes = [
  'https://www.googleapis.com/auth/plus.login',
  'https://www.googleapis.com/auth/plus.me',
	'https://www.googleapis.com/auth/userinfo.profile',
  'https://www.googleapis.com/auth/userinfo.email'
];

const url = oauth2Client.generateAuthUrl({
  // 'online' (default) or 'offline' (gets refresh_token)
  access_type: 'offline',
  // If you only need one scope you can pass it as a string
  scope: scopes
});



const routes = [
{
    method: 'GET',
    path: '/login',
    handler:  (request, reply) => {
    		return reply.redirect(url)
    }
},
{
    method: 'GET',
    path: '/oauthcallback',
    handler: function (request, reply) {
      async function getcode(){
        var code = request.query.code
        const {tokens} = await oauth2Client.getToken(code)
          oauth2Client.setCredentials(tokens)
            request.cookieAuth.set({ tokens });
            if (tokens.refresh_token) {
              // store the refresh_token in my database!
              console.log('refresh_token: '+ tokens.refresh_token);
              axios.get('https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token='+tokens.refresh_token)
              .then(function(data){
                var newUser = new UserModel({
                      "firstname": data.data.given_name,
                      "lastname": data.data.family_name,
                      "mobile": data.data.email,
                      "emailid": data.data.email,
                      "password": "N/A",
                      "address": "N/A",
                      "state": "N/A",
                      "city": "N/A",
                      "pincode": "N/A",
                      "gender": data.data.gender,
                      "lookingfor": "N/A",
                      "Status": "User",
                      "picture": data.data.picture.split('https').join('http')
                    });
                  UserModel.findOne({emailid: data.data.email})
                .then(function(result){
                  if (!result) {
                    request.cookieAuth.set(newUser);
                    return reply.redirect('/homepage/1')
                  }else{
                    request.cookieAuth.set(newUser);
                    return reply.redirect('/user/deshboard')
                  }
                });
              })
              .catch(error => {
                console.log(error);
              });
            }else{
              axios.get('https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token='+tokens.access_token)
              .then(function(data){
                var newUser = new UserModel({
                      "firstname": data.data.given_name,
                      "lastname": data.data.family_name,
                      "mobile": data.data.email,
                      "emailid": data.data.email,
                      "password": "N/A",
                      "address": "N/A",
                      "state": "N/A",
                      "city": "N/A",
                      "pincode": "N/A",
                      "gender": data.data.gender,
                      "lookingfor": "N/A",
                      "Status": "User",
                      "picture": data.data.picture.split('https').join('http')
                    });
                UserModel.findOne({emailid: data.data.email})
                .then(function(result){
                  if (!result) {
                    request.cookieAuth.set(newUser);
                    return reply.redirect('/homepage/1')
                  }else{
                    request.cookieAuth.set(newUser);
                    return reply.redirect('/user/deshboard')
                  }
                });
              })
              .catch(error => {
                console.log(error);
              });
            }
        }
        getcode();
      }
},
{
  method: 'GET',
  path: '/homepage/1',
//   config:{
//     auth:{
//       strategy: 'restricted'
//     }
//   },
  handler: function(request, reply){
    let authenticated_user = request.auth.credentials;
    var newUser = new UserModel(authenticated_user);
    newUser.save(function(err, data){
      if (err) {
        reply(err)
      }else{
        return reply.redirect('/user/deshboard')
      }
    })

  }
},
]
export default routes;