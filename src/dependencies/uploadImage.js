const AWS = require('aws-sdk');

//configuring the AWS environment
AWS.config.update({
  accessKeyId: "AKIAT7JZHDKCAWBLRN7M",
  secretAccessKey: "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"
});


const generateUID = () =>{
    // I generate the UID from two parts here
    // to ensure the random number provide enough bits.
    let firstPart = ((Math.random() * 46656) | 0).toString(36);
    let secondPart = ((Math.random() * 46656) | 0).toString(36);
    firstPart = ("000" + firstPart).slice(-3);
    secondPart = ("000" + secondPart).slice(-3);
    return firstPart + secondPart;
}

const uploadFileToS3 = (file, folder, content,  bucket='sherakart-001') => {
  // content would have like image/pdf/doc/xlsx/csv

  let s3, fileExtension, fileName, params;

  s3 = new AWS.S3();
  // file data
  // fileExtension = file.join(".")[file.join(".").length - 1];
  fileName = `${generateUID()}.png`
  //configuring parameters

  const options = {partSize: 10 * 1024 * 1024, queueSize: 1};

  params = {
    Bucket: bucket,
    Body: file,
    Key: `${folder}/${fileName}`,
    ContentType: `${content}/png`,
    ACL:"public-read"
  };

  // return a promises containing the link
  return s3.upload(params, options).promise()
    .then((data)=> {
        return Promise.resolve({
            fileLink: `https://${bucket}.s3.ap-south-1.amazonaws.com/${folder}/${fileName}`
        })
    })
    .catch(error => {
        console.error(error);
        throw error;
    })
}

const DeleteAwsImage = (folder, fileName, bucket='sherakart-001') => {
  var s3, params;
  s3 = new AWS.S3();
  // s3://sherakart/coverImageSlider/kvjo8h.png
  params = {
    Bucket: bucket,
    Key: `${folder}/${fileName}`
  };

  // return a promises containing the link
  return s3.deleteObject(params).promise()
    .then((data)=> {
        return Promise.resolve({
            Success: 'data deleted successfully'
        })
    })
    .catch(error => {
        console.error(error);
        throw error;
    })
}

module.exports = {
  uploadFileToS3, 
  DeleteAwsImage,
};
