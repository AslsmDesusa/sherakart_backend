'use strict'

import axios from 'axios';

const generateOTP = () =>{
    // I generate the UID from two parts here
    // to ensure the random number provide enough bits.
    let firstPart = ((Math.random() * 46656) | 0)
    let secondPart = ((Math.random() * 46656) | 0)
    // firstPart = ("000" + firstPart).slice(-3);
    // secondPart = ("000" + secondPart).slice(-3);
    return firstPart + secondPart
}

let sent_text = (firstName, lastName, phoneNumber, message) => {

    let OTP;

    OTP = `${generateOTP()}`

    message = `Message: Hi ${firstName } ${lastName}  Do not Share this code with anyone for security reasons Your unique verification code is: ${OTP}`
    
    let url = `http://zapsms.co.in/vendorsms/pushsms.aspx?user=Sherakat&password=sherakart&msisdn=${phoneNumber}&sid=SRAKRT&msg=${message}&fl=0&gwid=2`

    return axios.request(url)
    .then((data)=> {
        return Promise.resolve({
            Message: `Hi ${firstName } ${lastName} We have sent an OTP (One Time Password) to: ${phoneNumber} Please validate your phone number`,
            OTP: `${OTP}`
        })
    })
    .catch(error => {
        console.error(error.message);
        throw error.message;
    })
}
let WelcomeMSG = (phoneNumber, message) => {

    let url = `http://zapsms.co.in/vendorsms/pushsms.aspx?user=Sherakat&password=sherakart&msisdn=${phoneNumber}&sid=SRAKRT&msg=${message}&fl=0&gwid=2`

    return axios.request(url)
    .then((data)=> {
        return Promise.resolve({
            Message: `Your Account Successfully verified`
        })
    })
    .catch(error => {
        console.error(error.message);
        throw error.message;
    })
}

module.exports = {
    sent_text, 
    WelcomeMSG,
};